# Trading Symbols

A Northwestern Mutual Pre-Interview Technical Challenge

A solution by Andrew Wozniewicz, Optimax

April 30, 2018

## Description

You’ll be working with a cross-functional engineering team to deliver PoC’s and experiments; you’ll be the lead engineer. To demonstrate excellence in modern development tools and frameworks, we ask that you complete the following challenge. Please use this test to show off your skill set and what you can bring to the team. You will be critiqued on your quality, completeness, creativity, and technologies. If we proceed forward in the interviewing process, you will be asked to walk through your code. Choose <u>modern</u> technologies that exercise the breadth of approach and ones that you’re comfortable developing with. 

When you have completed the following challenge, place your code in a code repository, ex. github, bitbucket, dropbox, etc.

## The Problem

Create a responsive (phone, tablet, desktop) web application that allows the user to quick filter a list of things. The top of the page will have a search input field and then below that a list of things in response to the filter. The things should be sorted alphabetically.

The things could be anything, but should be AJAX pulled from a backend service that you write and should ultimately be pulled from an open public API.

Here’s an example list of API’s curated on GitHub, [https://github.com/toddmotto/public-apis](https://github.com/toddmotto/public-apis) but feel free to use any public API you wish.

## The Solution

`NMChallenge.sln` contains two projects:

- **NMChallenge** is the Web API backend service (ASP.NET MVC API project written in C#)
- **WebUI** is a website written in a combination of HTML 5, CSS 3, and ECMASCript 6.

The "items" being filtered are company ticker symbol + company name combination ultimately retrieved from a public web API at [https://api.iextrading.com/1.0/ref-data/symbols](https://api.iextrading.com/1.0/ref-data/symbols).

A working instance of the application can be found at [http://nmchallenge.optimax.com](http://nmchallenge.optimax.com). It makes use of the web API endpoint at [http://nmwebapi.optimax.com/api/symbols](http://nmwebapi.optimax.com/api/symbols).



