﻿using System;

namespace NMChallenge.Classes
{
    // https://api.iextrading.com/1.0/ref-data/symbols
    public class TickerSymbol
    {
        public string symbol { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public bool isEnabled { get; set; }
        public string type { get; set; }
        public int iexId { get; set; }
    }
}