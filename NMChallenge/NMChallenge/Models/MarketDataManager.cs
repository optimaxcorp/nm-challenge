﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NMChallenge.Classes;

namespace NMChallenge.Models
{
    public static class MarketDataManager
    {
        // Configuration
        private const string ConfigurationKey = "api:SourceURL";

        // Public API
        private static string SourceUrl => ConfigurationManager.AppSettings[ConfigurationKey];

        // Cached results
        private static IEnumerable<TickerSymbol> Symbols { get; set; }
        private static DateTime CachedTime { get; set; }


        #region Public Methods

        public static async Task<IEnumerable<TickerSymbol>> GetSymbols()
        {
            var now = DateTime.Now;
            if (CacheEmptyOrExpired(now))
            {
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(SourceUrl);
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = (HttpWebResponse) await request.GetResponseAsync())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    var json = await reader.ReadToEndAsync();
                    Symbols = JsonConvert.DeserializeObject<IEnumerable<TickerSymbol>>(json)
                        .OrderBy(s => s.symbol); // Just to make sure!
                    CachedTime = now;
                }
            }
            return Symbols;
        }



        private static bool CacheEmptyOrExpired(DateTime now)
        {
            return Symbols == null || CachedTime.AddDays(1) >= now;
        }


        #endregion
    }
}