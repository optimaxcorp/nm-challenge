﻿using NMChallenge.Classes;
using NMChallenge.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace NMChallenge.Controllers
{
    public class SymbolsController : ApiController
    {

        // GET api/symbols
        public async Task<IEnumerable<TickerSymbol>> Get()
        {
            var symbols = await MarketDataManager.GetSymbols();
            return symbols;
        }

    }
}
