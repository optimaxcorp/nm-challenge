﻿// Code-behind for index.html

class IndexPage {

    // Arguments are the ids of elements:
    //   resultsEltName - element where the resukts are displayed
    //   filterInputName - input element with the current filter string
    constructor(resultsEltName = 'results', filterInputName = 'filter') {
        this.allSymbols = [];
        this.selectedSymbols = [];
        this.filter = document.location.hash.substring(1);

        this.apiUrl = "http://localhost:36106/api/symbols";
        this.domResults = document.getElementById(resultsEltName);
        if (this.domResults === null)
            throw new `Element with the name "${resultsEltName}" does not exist in the DOM.`;
        this.domFilterInput = document.getElementById(filterInputName);
        if (this.domFilterInput === null)
            throw new `Element with the name "${filterInputName}" does not exist in the DOM.`;
        this.domFilterInput.value = this.filter;

        this.onFilterInput = this.onFilterInput.bind(this);
        this.domFilterInput.oninput = this.onFilterInput;
        this.getSymbolData();
    }


    // Retrieve the symbols via AJAX
    getSymbolData() {
        const req = new XMLHttpRequest();
        req.onload = this.getSymbolDataOnSuccess.bind(this);
        req.onerror = this.getSymbolDataOnError.bind(this);
        const bustBrowserCache = '?' + new Date().getTime();
        req.open('GET', this.apiUrl + bustBrowserCache, true);
        req.send();
    }


    // onload of getSymbolData AJAX request
    getSymbolDataOnSuccess(evt) {
        const xhr = evt.target;
        if (xhr.status < 200 || xhr.status >= 400) {
            console.error(xhr);
            return;
        }
        const json = xhr.responseText;
        this.allSymbols = JSON.parse(json);
        this.applyFilter(this.filter);
    }


    // onerror of getSymbolData AJAX request
    getSymbolDataOnError(evt) {
        const xhr = evt.target;
        console.error(xhr);
    }


    // React to the changes of filter string
    onFilterInput(e) {
        this.filter = e.target.value;
        this.applyFilter(this.filter);
    }


    // Given the filter string, filter the symbols for display
    applyFilter(filterValue) {
        filterValue = filterValue ? filterValue.toUpperCase() : null;
        this.selectedSymbols = filterValue ?
            this.selectedSymbols = this.allSymbols.filter(item => item.symbol.startsWith(filterValue)):
            this.selectedSymbols = this.allSymbols;
        this.updateUI(this.domResults);
    }


    // Update the UI DOM after changes in data selection
    updateUI(element) {
        element.innerHTML = '';
        this.updateCount(element);
        this.updateResults(element);
    }


    // Update the selected record counter
    updateCount(element) {
        var html = `<div class="counter">${this.selectedSymbols.length} symbols selected</div>`;
        element.innerHTML += html;
    }


    // Update the list of displayed symbols
    updateResults(element) {
        var html = '<table class="symbols">';
        for (var item of this.selectedSymbols) {
            let itemHtml =
                `<tr>
                    <td class="item-symbol">${item.symbol}</td>
                    <td class="item-name">${item.name}</td>
                 </tr>`;
            html += itemHtml;
        };
        html += '</table>';
        element.innerHTML += html;
    }

}
